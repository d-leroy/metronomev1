package model;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import view.IButton;
import view.IDisplay;
import view.ISlider;
import controler.Controler;

public class MetronomeEngineTest {

	private MetronomeEngine me;
	private Controler ctl;

	@Before
	public void setUp() throws Exception {
		me = new MetronomeEngine();
		ctl = new Controler();

		ISlider sliderMock = mock(ISlider.class);
		when(sliderMock.getValue()).thenReturn(0.18f);
		IButton startButtonMock = mock(IButton.class);
		IButton stopButtonMock = mock(IButton.class);
		IDisplay displayMock = mock(IDisplay.class);

		ctl.sliderInit(sliderMock);
		ctl.startButtonInit(startButtonMock);
		ctl.stopButtonInit(stopButtonMock);
		ctl.displayInit(displayMock);

		ctl.init(me);
	}

	@After
	public void tearDown() throws Exception {
		ctl = null;
		me = null;
	}

	@Test
	public void initTest1() {
		assertEquals(me.getBpm(),59);
		assertEquals(me.getBpb(),4);
	}

	@Test
	public void setBpmTest1() {
		me.setBpm(0);
		assertEquals(me.getBpm(),59);
	}

	@Test
	public void setBpmTest2() {
		me.setBpm(300);
		assertEquals(me.getBpm(),59);
	}

	@Test
	public void setBpmTest3() {
		me.setBpm(120);
		assertEquals(me.getBpm(),120);
	}

	@Test
	public void setBpbTest1() {
		me.setBpb(1);
		assertEquals(me.getBpb(),4);
	}

	@Test
	public void setBpbTest2() {
		me.setBpb(8);
		assertEquals(me.getBpb(),4);
	}

	@Test
	public void setBpbTest3() {
		me.setBpb(5);
		assertEquals(me.getBpb(),5);
	}

	private int nbBeat;
	private int nbBar;
	private int timesExpected;
	private int bpm;
	private int bpb;
	
	@Test
	public void setBarHandlerTest1() {
		nbBeat = 0; nbBar = 0; bpm = 60;
		bpb = 4; timesExpected = 10;
		me.setBeatEventHandler(() -> nbBeat++);
		me.setBarEventHandler(() -> nbBar++);
		me.setBpm(bpm);
		me.setBpb(bpb);
		me.setRunning(true);
		try {
			Thread.sleep(timesExpected * (60000 / bpm));
			assertEquals(timesExpected, nbBeat);
			int nbBarExpected;
			if(timesExpected % bpb == 0)
				nbBarExpected = timesExpected / bpb;
			else
				nbBarExpected = 1 + timesExpected / bpb;
			assertEquals(nbBarExpected, nbBar);
		}
		catch (InterruptedException e) {
			assert(false);
			e.printStackTrace();
		}
		finally {
			me.setRunning(false);
		}
	}


	@Test
	public void setBarHandlerTest2() {
		nbBeat = 0; nbBar = 0; bpm = 120;
		bpb = 4; timesExpected = 10;
		me.setBeatEventHandler(() -> nbBeat++);
		me.setBarEventHandler(() -> nbBar++);
		me.setBpm(bpm);
		me.setBpb(bpb);
		me.setRunning(true);
		try {
			Thread.sleep(timesExpected * (60000 / bpm));
			assertEquals(timesExpected, nbBeat);
			int nbBarExpected;
			if(timesExpected % bpb == 0)
				nbBarExpected = timesExpected / bpb;
			else
				nbBarExpected = 1 + timesExpected / bpb;
			assertEquals(nbBarExpected, nbBar);
		}
		catch (InterruptedException e) {
			assert(false);
			e.printStackTrace();
		}
		finally {
			me.setRunning(false);
		}
	}
	
	@Test
	public void setBarHandlerTest3() {
		nbBeat = 0; nbBar = 0; bpm = 240;
		bpb = 4; timesExpected = 10;
		me.setBeatEventHandler(() -> nbBeat++);
		me.setBarEventHandler(() -> nbBar++);
		me.setBpm(bpm);
		me.setBpb(bpb);
		me.setRunning(true);
		try {
			Thread.sleep(timesExpected * (60000 / bpm));
			assertEquals(timesExpected, nbBeat);
			int nbBarExpected;
			if(timesExpected % bpb == 0)
				nbBarExpected = timesExpected / bpb;
			else
				nbBarExpected = 1 + timesExpected / bpb;
			assertEquals(nbBarExpected, nbBar);
		}
		catch (InterruptedException e) {
			assert(false);
			e.printStackTrace();
		}
		finally {
			me.setRunning(false);
		}
	}
}
