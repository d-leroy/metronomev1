package model;

import share.ICommand;

/**
 * 
 * Moteur de l'application métronome
 * @author LAVAURE Caroline & LEROY Dorian
 * 
 */
public class MetronomeEngine implements IMetronomeEngine {

	private int bpm;
	private int bpb;
	private boolean running;
	private ICommand beatEventHandler;
	private ICommand barEventHandler;
	private ICommand runningEventHandler;
	private ICommand bpmChangeEventHandler;

	private boolean destroy = false;

	private void run() {
		Thread t = new Thread(new Runnable() {
			int i = bpb;
			@Override
			public void run() {
				while (!destroy) {
					beatEventHandler.execute();
					if (i==bpb) {
						barEventHandler.execute();
					}
					i--;
					if (i==0) {
						i = bpb;
					}
					try {
						Thread.sleep(60000/bpm);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		// configurer le thread en daemon permet de s'assurer
		// que le thread s'arrête si l'on ferme la fenetre avant
		// de cliquer sur le bouton stop.
		t.setDaemon(true);
		t.start();

	}


	@Override
	public int getBpm() {
		return bpm;
	}
	
	@Override
	public void setBpm(int bpm) {
		if(bpm >= 20 && bpm <= 240) {
			this.bpm = bpm;
			bpmChangeEventHandler.execute();
		}
	}
	
	@Override
	public int getBpb() {
		return bpb;
	}
	
	@Override
	public void setBpb(int bpb) {
		if(bpb >= 2 && bpb <= 7) {
			this.bpb = bpb;
		}
	}

	@Override
	public boolean isRunning() {
		return running;
	}


	@Override
	public void setRunning(boolean running) {
		if(this.running != running) {
			this.running = running;
			runningEventHandler.execute();
		}
		if(this.running) {
			destroy = false;
			this.run();
		}
		else {
			destroy = true;
		}
	}
	@Override
	public ICommand getRunningEventHandler() {
		return runningEventHandler;
	}
	@Override
	public void setRunningEventHandler(ICommand runningEventHandler) {
		this.runningEventHandler = runningEventHandler;
	}


	@Override
	public ICommand getBeatEventHandler() {
		return beatEventHandler;
	}
	@Override
	public void setBeatEventHandler(ICommand beatEventHandler) {
		this.beatEventHandler = beatEventHandler;
	}

	@Override
	public ICommand getBarEventHandler() {
		return barEventHandler;
	}
	@Override
	public void setBarEventHandler(ICommand barEventHandler) {
		this.barEventHandler = barEventHandler;
	}

	@Override
	public ICommand getBpmChangeEventHandler() {
		return bpmChangeEventHandler;
	}
	@Override
	public void setBpmChangeEventHandler(ICommand bpmChangeEventHandler) {
		this.bpmChangeEventHandler = bpmChangeEventHandler;
	}

}
