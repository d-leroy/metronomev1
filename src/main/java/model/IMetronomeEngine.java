package model;

import share.ICommand;

/**
 * 
 * Moteur de l'application métronome
 * @author LAVAURE Caroline & LEROY Dorian
 * 
 */
public interface IMetronomeEngine {

	/**  
	 * Récupération du bpm.
	 */
	public abstract int getBpm();

	/**
	 * Mise à jour du bpm
	 * @param bpm
	 */
	public abstract void setBpm(int bpm);

	/**
	 * Récuperation du bpb.
	 */
	public abstract int getBpb();

	/**
	 * Mise à jour du bpb
	 * @param bpb
	 */
	public abstract void setBpb(int bpb);

	/**
	 * Verification que le métronome est en cours d'execution.
	 */
	public abstract boolean isRunning();

	/**
	 * Exécute la commande runningEvent @see Controller.handleRunningEvent().
	 */
	public abstract void setRunning(boolean running);


	/**
	 * Récupération de la commande beatEventHandler
	 * @see Controller.handleBeatEvent() 
	 * @see Controler.init(IMetronomeEngine me)
	 */
	public abstract ICommand getBeatEventHandler();

	/**
	 * Mise à jour de la commande beatEventHandler
	 * (déclenche le speaker et le flash de bpmLed) 
	 * @see Controller.handleBeatEvent()
	 * @see Controler.init(IMetronomeEngine me)
	 */
	public abstract void setBeatEventHandler(ICommand beatEventHandler);

	/**
	 * Récupération de la commande barEventHandler
	 * @see Controller.handleBarEvent()
	 * @see Controler.init(IMetronomeEngine me)
	 */
	public abstract ICommand getBarEventHandler();

	/**
	 * Mise à jour de la commande barEventHandler
	 * @see Controler.handleBarEvent()
	 * @see Controler.init(IMetronomeEngine me)
	 */
	public abstract void setBarEventHandler(ICommand barEventHandler);

	/**
	 * Récupération de la commande BpmChangeEventHandler
	 * @see Controler.handleBpmChange()
	 * @see Controler.init(IMetronomeEngine me)
	 */
	public abstract ICommand getBpmChangeEventHandler();

	/**
	 * Mise a jour de la commande BpmChangeEventHandler
	 * @see Controler.handleBpmChange()
	 * @see Controler.init(IMetronomeEngine me)
	 * @param bpmChangeEventHandler
	 */
	public abstract void setBpmChangeEventHandler(ICommand bpmChangeEventHandler);

	/**
	 * Récupération de la commande runningEventHandler()
	 * @see Controler.handleRunningEvent()
	 * @see Controler.init(IMetronomeEngine me) 
	 */
	public ICommand getRunningEventHandler();
	
	/**
	 * Mise à jour de la commande runningEventHandler()
	 * @see Controler.handleRunningEvent()
	 * @see Controler.init(IMetronomeEngine me) 
	 */
	public void setRunningEventHandler(ICommand runningEventHandler);

}