package view;

import share.ICommand;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public interface IButton {
	/**
	 * Mise à jour de la commande associée au bouton.
	 * @param onClickHandler
	 */
	public void setOnClickHandler(ICommand onClickHandler);

	/**
	 * Active ou désactive le bouton.
	 * @param b : boolean
	 */
	public void setEnable(boolean b);
}
