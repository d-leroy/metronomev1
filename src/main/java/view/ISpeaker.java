package view;

/**
 * 
 * Effet sonore du métronome
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */	
public interface ISpeaker {
	/**
	 * Déclenche l'effet sonore du métronome.
	 */
	public void click();
}
