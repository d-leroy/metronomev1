package view;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public interface IDisplay {
	
	/**
	 * Mise à jour du texte affiché.
	 */
	public void setValue(String bpm);
}
