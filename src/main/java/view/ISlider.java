package view;

import share.ICommand;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public interface ISlider {
	/**
	 * Récupère la valeur associée au slider.
	 */
	public float getValue();
	
	/**
	 * Mise a jour de la commande onChangeHandler associée au slider
	 * @param onChangeHandler
	 */
	public void setOnChangeHandler(ICommand onChangeHandler);
	
	/**
	 * Active ou désactive le slider.
	 * @param enabled
	 */
	public void setEnabled(boolean enabled);

}
