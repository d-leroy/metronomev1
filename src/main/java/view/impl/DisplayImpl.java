package view.impl;

import javafx.scene.text.Text;
import view.IDisplay;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public class DisplayImpl implements IDisplay {

	private Text text;
	
	/**
	 * Constructeur.
	 * @param text
	 */
	public DisplayImpl(Text text) {
		this.text = text;
	}
	
	@Override
	public void setValue(String bpm) {
		this.text.setText(bpm);
	}

}
