package view.impl;

import javafx.scene.media.AudioClip;
import view.ISpeaker;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public class SpeakerImpl implements ISpeaker {

	private AudioClip ac;

	/**
	 * Constructeur
	 */
	public SpeakerImpl() {
		ac = new AudioClip(""+getClass().getResource("/click.wav"));
	}

	@Override
	public void click() {
		ac.play();
	}

}
