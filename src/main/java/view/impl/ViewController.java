package view.impl;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import controler.Controler;
import model.MetronomeEngine;

/**
 * Contrôleur JAVAFX de la vue.
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 */
public class ViewController implements Initializable {

	@FXML
	private Button startBtn;
	@FXML
	private Button stopBtn;
	@FXML
	private Button decBtn;
	@FXML
	private Button incBtn;
	@FXML
	private Text display;
	@FXML
	private Slider slider;
	@FXML
	private Circle ledBpm;
	@FXML
	private Circle ledBpb;
	@FXML
	private Rectangle rectEffet;
		
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
			
		slider.setMin(0);
		slider.setMax(1);
		slider.setValue(0.18);
				
		Controler ctl = new Controler();
		ButtonImpl startButton = new ButtonImpl(startBtn);
		ButtonImpl stopButton = new ButtonImpl(stopBtn);
		ButtonImpl incButton = new ButtonImpl(incBtn);
		ButtonImpl decButton = new ButtonImpl(decBtn);
		SliderImpl slideri = new SliderImpl(slider);
		DisplayImpl displayi = new DisplayImpl(display);
		LedImpl ledBpmi = new LedImpl(ledBpm);
		LedImpl ledBpbi = new LedImpl(ledBpb);
		SpeakerImpl speakeri = new SpeakerImpl();
		RectangleImpl reci = new RectangleImpl(rectEffet);
				
		ctl.startButtonInit(startButton);
		ctl.stopButtonInit(stopButton);
		ctl.incButtonInit(incButton);
		ctl.decButtonInit(decButton);
		ctl.sliderInit(slideri);
		ctl.displayInit(displayi);
		ctl.speakerInit(speakeri);
		ctl.bpmLedInit(ledBpmi);
		ctl.bpbLedInit(ledBpbi);

		ctl.init(new MetronomeEngine());
		
	}
	
}
