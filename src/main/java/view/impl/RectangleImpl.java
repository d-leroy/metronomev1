package view.impl;

import javafx.scene.effect.Reflection;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;

/**
 * Background du métronome avec un effet de reflection.
 */
public class RectangleImpl {

	private Rectangle rect;

	/**
	 * Constructeur.
	 * @param rect
	 */
	public RectangleImpl(Rectangle rect) {
		this.rect = rect;
		
		this.rect.setFill(// Un joli dégardé
				new LinearGradient(0f, 0f, 0f, 1f, true, CycleMethod.NO_CYCLE,
						new Stop[] {
						new Stop(0, Color.web("#AFAFAF")),
						new Stop(1, Color.web("#CECECE"))
				}
						)
				);
		
		Reflection r = new Reflection(); 	//Un effet de réflection
		r.setFraction(0.25);
		r.setBottomOpacity(0);
		r.setTopOpacity(0.5);
		this.rect.setEffect(r);
	}
	
}
