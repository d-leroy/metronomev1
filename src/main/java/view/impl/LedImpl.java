package view.impl;


import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import view.ILed;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public class LedImpl implements ILed {

	private Circle led = new Circle();

	/**
	 * Constructeur.
	 * @param led
	 */
	public LedImpl(Circle led) {
		this.led = led;
	}

	@Override
	public void flash() {

		Task<Void> task = new Task<Void>() {
			@Override protected Void call() throws Exception {
				Platform.runLater(new Runnable() {
					@Override public void run() {
						led.setFill(Color.WHITE);
					}

				});
				Thread.sleep(100);
				Platform.runLater(new Runnable() {
					@Override public void run() {
						led.setFill(Color.RED);
					}

				});
				return null;
			}
		};
		Thread th = new Thread(task);
		th.start();
	}

}
