package view.impl;

import javafx.scene.control.Button;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import share.ICommand;
import view.IButton;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public class ButtonImpl implements IButton {

	private Button button;
	private ICommand onClickHandler;

	/**
	 * Constructeur.
	 * @param button
	 */
	public ButtonImpl(Button button) {
		this.button = button;
		this.button.setOnAction((e) -> click());
		Light.Distant light = new Light.Distant(); // effet sur les boutons
		light.setAzimuth(-135.0);
		Lighting li = new Lighting();
		li.setLight(light);
		this.button.setEffect(li);
	}

	@Override
	public void setOnClickHandler(ICommand onClickHandler) {
		this.onClickHandler = onClickHandler;
	}

	/**
	 * Excécute la commande associée au bouton.
	 */
	private void click() {
		if(this.onClickHandler != null) {
			this.onClickHandler.execute();
		}
	}


	@Override
	public void setEnable(boolean b) {
		this.button.setDisable(!b);
	}
}
