package view.impl;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.Slider;
import share.ICommand;
import view.ISlider;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public class SliderImpl implements ISlider {

	private ICommand onChangeHandler;
	private Slider slider;
	@SuppressWarnings("rawtypes")
	private ChangeListener changeListener = (a,b,c) -> changed();
	
	private boolean enabled;
	
	/**
	 * Constructeur.
	 * @param slider
	 */
	public SliderImpl(Slider slider) {
		this.slider = slider;
	}

	@Override
	public float getValue() {
		return slider.valueProperty().floatValue();
	}

	@Override
	public void setOnChangeHandler(ICommand onChangeHandler) {
		this.onChangeHandler = onChangeHandler;
	}

	/**
	 * Exécute la commande associée au slider.
	 */
	private void changed() {
		this.onChangeHandler.execute();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setEnabled(boolean enabled) {
		if(this.enabled != enabled) {
			if(enabled) {
				this.slider.valueProperty().addListener(changeListener);
				changed();
			}
			else {
				this.slider.valueProperty().removeListener(changeListener);
			}
		}
	}

}
