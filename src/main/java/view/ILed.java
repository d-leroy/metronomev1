package view;

/**
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public interface ILed {	
	/**
	 * Allume la led.
	 */
	public void flash();
}
