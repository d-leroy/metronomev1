package controler;

import model.IMetronomeEngine;
import view.IButton;
import view.IDisplay;
import view.ILed;
import view.ISlider;
import view.ISpeaker;

/**
 * 
 * Toutes les communications entre la vue et le modèle
 * passent par la classe Controler.
 * 
 * @author LAVAURE Caroline & LEROY Dorian
 *
 */
public class Controler {

	private IMetronomeEngine me;
	private IButton startButton;
	private IButton stopButton;
	private IButton incButton;
	private IButton decButton;
	private ILed bpmLed; // tempo
	private ILed bpbLed; // mesure
	private ISlider slider;
	private ISpeaker speaker;
	private IDisplay display;

	/**
	 * Déclenche le flash de bmpLed et le tick de speaker.
	 */
	public void handleBeatEvent() {
		bpmLed.flash();
		speaker.click();
	}
	
	/**
	 * Initialisation de speaker.
	 * @param speaker
	 */
	public void speakerInit(ISpeaker speaker) {
		this.speaker = speaker;
	}
	
	/**
	 * Initialisation de bpmLed.
	 * @param bpmLed
	 */
	public void bpmLedInit(ILed bpmLed) {
		this.bpmLed = bpmLed;
	}

	/**
	 * Déclenche le flash de bpbLed.
	 */
	public void handleBarEvent() {
		bpbLed.flash();
	}
	
	/**
	 * Initialisation de bpbLed.
	 * @param bpbLed
	 */
	public void bpbLedInit(ILed bpbLed) {
		this.bpbLed = bpbLed;
	}

	/**
	 * Mise en marche du métronome s'il est arrêté.
	 * Arrêt du métronome s'il est en marche.
	 * Met à jour l'affichage en conséquence.
	 */
	public void handleRunningEvent() {
		if(me.isRunning()) {
			startButton.setEnable(false);
			stopButton.setEnable(true);
			display.setValue("" + me.getBpm());
			slider.setEnabled(true);
		}
		else {
			startButton.setEnable(true);
			stopButton.setEnable(false);
			display.setValue("OFF");
			slider.setEnabled(false);
		}
	}

	/**
	 * Récupère la nouvelle valeur du tempo et met à jour 
	 * la valeur de l'affichage avec cette valeur.
	 */
	public void handleBpmChange() {
		if(me.isRunning()) {
			display.setValue("" + me.getBpm());
		}
	}

	/**
	 * Initialisation de l'affichage.
	 * @param display
	 */
	public void displayInit(IDisplay display) {
		this.display = display;
	}

	/**
	 * Récupère la nouvelle valeur du slider et
	 * recalcule le tempo à partir de cette valeur.
	 */
	public void handleSliderChanged() {
		me.setBpm((int) (220 * slider.getValue())+ 20); 
	}
	
	/**
	 * Initialisation du slider et de la commande qui lui est associée.
	 * @param slider
	 */
	public void sliderInit(ISlider slider) {
		this.slider = slider;
		this.slider.setOnChangeHandler(() -> handleSliderChanged());
	}

	/**
	 * Démarre le métronome.
	 */
	public void handleStartClicked() {
		me.setRunning(true);
	}
	
	/**
	 * Initialisation du bouton start et de la commande qui lui est asssociée.
	 * @param button
	 */
	public void startButtonInit(IButton button) {
		this.startButton = button;
		startButton.setOnClickHandler(() -> handleStartClicked());
	}

	/**
	 * Arrête le métronome.
	 */
	public void handleStopClicked() {
		me.setRunning(false);
	}
	
	/**
	 * Initialisation du bouton stop et de la commande qui lui est associée. 
	 * @param button
	 */
	public void stopButtonInit(IButton button) {
		this.stopButton = button;
		stopButton.setOnClickHandler(() -> handleStopClicked());
	}

	/**
	 * Incrémente le bpb de 1.
	 */
	public void handleIncClicked() {
		me.setBpb(me.getBpb() + 1);
	}
	
	/**
	 * Initialisation du bouton inc et de la commande qui lui est asssociée.
	 * @param button
	 */
	public void incButtonInit(IButton button) {
		this.incButton = button;
		incButton.setOnClickHandler(() -> handleIncClicked());
	}

	/**
	 * Decrémente le bpb de 1.
	 */
	public void handleDecClicked() {
		me.setBpb(me.getBpb() - 1);
	}
	
	/**
	 * Initialisation du bouton dec et de la commande qui lui est asssociée.
	 * @param button
	 */
	public void decButtonInit(IButton button) {
		this.decButton = button;
		decButton.setOnClickHandler(() -> handleDecClicked());
	}

	/**
	 * Initialisation du metronome.	  
	 * @param me 
	 */
	public void init(IMetronomeEngine me) {
		this.me = me;
		me.setBeatEventHandler(() -> handleBeatEvent());
		me.setBarEventHandler(() -> handleBarEvent());
		me.setBpmChangeEventHandler(() -> handleBpmChange());
		me.setRunningEventHandler(() -> handleRunningEvent());
		me.setBpm((int) (220 * slider.getValue())+ 20);
		me.setBpb(4);
	}


}
