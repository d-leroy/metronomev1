package main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application{
	
	public static void main(String[] args) {
		launch(args);
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/metronome.fxml"));
		
        Scene scene = new Scene(root, 500, 400);
    
        primaryStage.setTitle("MetronomeV1");
        primaryStage.setScene(scene);
        primaryStage.show();
		
	}

	

}
