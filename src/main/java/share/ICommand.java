package share;

/**
 * 
  * @author Lavaure Caroline & LEROY Dorian
 *
 */
public interface ICommand {

	public void execute();
}
